package fab;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class brokenlinks {

	public static void main(String[] args) throws MalformedURLException, IOException {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/chromedriver"); 
		
		
		WebDriver driver = new ChromeDriver();
/*lists of all links*/
		driver.get("https://doubtnut.com/");
		List<WebElement>linkslist= driver.findElements(By.tagName("a"));
		linkslist.addAll(driver.findElements(By.tagName("img")));

		//for(WebElement it: linkslist) {
			//System.out.println("for each loop " +it.getText());
	//	}
		System.out.println("All links and images" + linkslist.size());
		
		for(int i=0; i<linkslist.size(); i++)
		{
		System.out.println(linkslist.get(i).getAttribute("href"));
		}
		
		
		List<WebElement> activelinks = new ArrayList<WebElement>();
		
		//iterate through linklinks and exclude links that don't have href
		
		
		for(int i=0;i<linkslist.size();i++) {
			
			//System.out.println(linkslist.get(i).getAttribute("href"));
			
			if (linkslist.get(i).getAttribute("href")!=null && (!linkslist.get(i).getAttribute("href").contains("javascript:void(0)"))); 
			{
				activelinks.add(linkslist.get(i));
				
			}
		}

//get size of active links
		System.out.println("active links and images" +activelinks.size());
		
		//check the href url,with http connection api
		
		for(int j=0;j<activelinks.size();j++) {
		
			HttpURLConnection connection=(HttpURLConnection) new URL(activelinks.get(j).getAttribute("href")).openConnection();
		 connection.connect();
		int res=  connection.getResponseCode();
		  String response=connection.getResponseMessage();
		 connection.disconnect();
		 System.out.println(activelinks.get(j).getAttribute("href") +"-->" +response);
		 System.out.println(activelinks.get(j).getAttribute("href") +"-->" +res);
		}
	}

}
